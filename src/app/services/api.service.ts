import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { Comment } from '../models/comment';
import { Post } from '../models/post';

@Injectable({
	providedIn: 'root',
})
export class ApiService {
	baseUrl = environment.baseUrl;
	constructor(private http: HttpClient) {}

	getPosts(): Observable<Post[]> {
		return this.http.get<Post[]>(`${this.baseUrl}/posts`);
	}

	getPostById(postId: number): Observable<Post> {
		return this.http.get<Post>(`${this.baseUrl}/posts/${postId}`);
	}

	getComments(postId?: number): Observable<Comment[]> {
		let params = new HttpParams();
		if (postId !== undefined) params = params.set('postId', postId.toString());
		return this.http.get<Comment[]>(`${this.baseUrl}/comments`, { params });
	}

	addComment(postId: number, comment: { author: string; content: string; date: string }): Observable<Comment> {
		return this.http.post<Comment>(`${this.baseUrl}/posts/${postId}/comments`, comment);
	}
}
