import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { ApiService } from './api.service';
import { Comment } from '../models/comment';
import { Post } from '../models/post';

export const mockPosts: Post[] = [
	{
		id: 1,
		title: 'Blog post #1',
		author: 'Melissa Manges',
		publish_date: '2016-02-23',
		slug: 'blog-post-1',
		description: 'Some desc. 1',
		content: '<p>Some blog content 1</p>',
	},
	{
		id: 2,
		title: 'Blog post #2',
		author: 'Olene Ogan',
		publish_date: '2016-05-13',
		slug: 'blog-post-2',
		description: 'Some desc. 2',
		content: '<p>Some blog content 2</p>',
	},
];

export const mockComments: Comment[] = [
	{ id: 1, postId: 1, parent_id: null, user: 'Amelia', date: '2016-02-23', content: 'Post content 1' },
	{ id: 2, postId: 1, parent_id: 1, user: 'Jake', date: '2016-02-23', content: 'Post content 2' },
	{ id: 3, postId: 2, parent_id: null, user: 'Sheila', date: '2016-02-25', content: 'Post content 3' },
];

describe('ApiService', () => {
	let service: ApiService;
	let mock: HttpTestingController;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [HttpClientTestingModule],
			providers: [ApiService],
		});
		service = TestBed.inject(ApiService);
		mock = TestBed.inject(HttpTestingController);
	});

	afterEach(() => {
		mock.verify();
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});

	describe('getPosts', () => {
		it('should return an Observable<Post[]>', () => {
			service.getPosts().subscribe((posts) => {
				expect(posts.length).toBe(mockPosts.length);
				expect(posts[0].id).toBe(mockPosts[0].id);
				expect(posts[0].author).toBe(mockPosts[0].author);
			});

			const req = mock.expectOne(`${service.baseUrl}/posts`);
			expect(req.request.method).toBe('GET');
			req.flush(mockPosts);
		});
	});

	describe('getComments', () => {
		it('should return an Observable<Comment[]>', () => {
			service.getComments().subscribe((comments) => {
				expect(comments.length).toBe(3);
				expect(comments[0].id).toBe(1);
				expect(comments[0].parent_id).toBeNull();
				expect(comments[1].parent_id).toBe(1);
				expect(comments[1].user).toBe('Jake');
				expect(comments[2].date).toBe('2016-02-25');
			});

			const req = mock.expectOne(`${service.baseUrl}/comments`);
			expect(req.request.method).toBe('GET');
			req.flush(mockComments);
		});

		it('should be filterable by postId', () => {
			const mockFilteredComments: Comment[] = [
				{ id: 3, postId: 2, parent_id: null, user: 'Sheila', date: '2016-02-25', content: 'Post content 3' },
			];

			service.getComments(2).subscribe((comments) => {
				expect(comments.length).toBe(1);
				expect(comments[0].id).toBe(3);
				expect(comments[0].user).toBe('Sheila');
			});

			const req = mock.expectOne(`${service.baseUrl}/comments?postId=2`);
			expect(req.request.method).toBe('GET');
			req.flush(mockFilteredComments);
		});
	});
});
