import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Type } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { of } from 'rxjs';
import { ApiService } from 'src/app/services/api.service';
import { mockPosts } from 'src/app/services/api.service.spec';
import { PostListComponent } from './post-list.component';

describe('PostListComponent', () => {
	let component: PostListComponent;
	let fixture: ComponentFixture<PostListComponent>;
	let api: ApiService;
	let mock: HttpTestingController;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [PostListComponent],
			imports: [HttpClientTestingModule],
			providers: [ApiService],
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(PostListComponent);
		component = fixture.componentInstance;
		api = fixture.debugElement.injector.get(ApiService);
		mock = fixture.debugElement.injector.get<HttpTestingController>(HttpTestingController as Type<HttpTestingController>);
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('should populate view with posts from the api', () => {
		spyOn(api, 'getPosts').and.returnValue(of(mockPosts));
		fixture.componentInstance.$posts = api.getPosts();
		fixture.componentInstance.$posts.subscribe((posts) => {
			fixture.detectChanges();
			expect(posts.length).toBe(mockPosts.length);
			const blogPosts = fixture.debugElement.queryAll(By.css('.post-list-item-container'));
			expect(blogPosts.length).toBe(mockPosts.length);
		});
	});

	it('should show message when no posts exist', () => {
		spyOn(api, 'getPosts').and.returnValue(of([]));
		fixture.componentInstance.$posts = api.getPosts();
		fixture.componentInstance.$posts.subscribe((posts) => {
			fixture.detectChanges();
			expect(posts.length).toBe(0);
			const noPostMessage = fixture.debugElement.query(By.css('.no-posts-message'));
			expect(noPostMessage.nativeElement.innerText).toBe('No posts found.');
		});
	});
});
