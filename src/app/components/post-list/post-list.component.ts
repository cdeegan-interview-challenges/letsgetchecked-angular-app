import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Post } from 'src/app/models/post';

@Component({
	selector: 'app-post-list',
	templateUrl: './post-list.component.html',
	styleUrls: ['./post-list.component.scss'],
})
export class PostListComponent implements OnInit {
	$posts = this.api.getPosts();
	constructor(private api: ApiService) {}

	ngOnInit(): void {}

	sortPostsByDate(posts: Post[]): Post[] {
		return posts.sort((a, b) => {
			return new Date(b.publish_date).getTime() - new Date(a.publish_date).getTime();
		});
	}
}
