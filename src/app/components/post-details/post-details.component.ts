import { NestedTreeControl } from '@angular/cdk/tree';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Comment } from 'src/app/models/comment';
import { CommentNode } from 'src/app/models/comment-node';
import { Post } from 'src/app/models/post';
import { ApiService } from 'src/app/services/api.service';

@Component({
	selector: 'app-post-details',
	templateUrl: './post-details.component.html',
	styleUrls: ['./post-details.component.scss'],
})
export class PostDetailsComponent implements OnInit {
	formSubmitting = false;
	postId: number;
	$post: Observable<Post>;
	$comments: Observable<Comment[]>;
	commentTreeControl = new NestedTreeControl<CommentNode>((node) => node.children);
	commentTreeDataSource = new MatTreeNestedDataSource<CommentNode>();
	newCommentForm: FormGroup;
	treeNodeHasChild = (_: number, node: CommentNode) => !!node.children && node.children.length > 0;
	constructor(private route: ActivatedRoute, private api: ApiService, private formBuilder: FormBuilder) {}

	get comment(): AbstractControl {
		return this.newCommentForm.get('comment');
	}

	ngOnInit(): void {
		this.postId = parseInt(this.route.snapshot.paramMap.get('id'), 10);
		this.$post = this.api.getPostById(this.postId);
		this.$comments = this.api
			.getComments(this.postId)
			.pipe(tap((comments) => (this.commentTreeDataSource.data = this.getCommentTreeData(comments))));

		this.newCommentForm = this.formBuilder.group({
			comment: ['', [Validators.required, Validators.minLength(10)]],
		});
	}

	getCommentTreeData(comments: Comment[]): CommentNode[] {
		const hashTable = Object.create(null);
		comments.forEach((comment) => (hashTable[comment.id] = { ...comment, children: [] }));
		const dataTree = [];
		comments.forEach((comment) => {
			if (comment.parent_id) hashTable[comment.parent_id].children.push(hashTable[comment.id]);
			else dataTree.push(hashTable[comment.id]);
		});
		return dataTree;
	}

	onNewCommentSubmit(form: FormGroup): void {
		if (!form.valid) return;
		const date = new Date();
		this.formSubmitting = true;
		this.api
			.addComment(this.postId, {
				author: 'Anonymous',
				content: this.comment.value,
				date: `${date.getFullYear()}-${date.getMonth().toString().padStart(2, '0')}-${date.getDate().toString().padStart(2, '0')}`,
			})
			.subscribe(() => {
				this.formSubmitting = false;
				// shortcut to re-render page
				window.location.reload();
			});
	}
}
