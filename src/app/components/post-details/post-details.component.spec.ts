import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Type } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { of } from 'rxjs';
import { ApiService } from 'src/app/services/api.service';
import { mockComments, mockPosts } from 'src/app/services/api.service.spec';
import { PostDetailsComponent } from './post-details.component';

describe('PostDetailsComponent', () => {
	let component: PostDetailsComponent;
	let fixture: ComponentFixture<PostDetailsComponent>;
	let api: ApiService;
	let mock: HttpTestingController;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [RouterModule.forRoot([]), HttpClientTestingModule],
			providers: [FormBuilder, ApiService],
			declarations: [PostDetailsComponent],
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(PostDetailsComponent);
		component = fixture.componentInstance;
		api = fixture.debugElement.injector.get(ApiService);
		mock = fixture.debugElement.injector.get<HttpTestingController>(HttpTestingController as Type<HttpTestingController>);
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('should populate view with post from the api', () => {
		spyOn(api, 'getPostById').and.returnValue(of(mockPosts[0]));
		fixture.componentInstance.$post = api.getPostById(1);
		fixture.componentInstance.$post.subscribe((post) => {
			fixture.detectChanges();
			expect(post.id).toBe(mockPosts[0].id);
		});
	});

	it('should populate view with comments from the api', () => {
		const filteredMockComments = mockComments.filter((c) => c.postId === mockPosts[0].id);
		spyOn(api, 'getComments').and.returnValue(of(filteredMockComments));
		fixture.componentInstance.$comments = api.getComments(mockPosts[0].id);
		fixture.componentInstance.$comments.subscribe((comments) => {
			fixture.detectChanges();
			expect(comments[0].id).toBe(filteredMockComments[0].id);
		});
	});
});
