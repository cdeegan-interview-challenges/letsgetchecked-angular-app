import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CssTaskComponent } from './css-task.component';

describe('CssTaskComponent', () => {
	let component: CssTaskComponent;
	let fixture: ComponentFixture<CssTaskComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [CssTaskComponent],
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(CssTaskComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
