import { Component, HostListener, OnInit } from '@angular/core';

@Component({
	selector: 'app-css-task',
	templateUrl: './css-task.component.html',
	styleUrls: ['./css-task.component.scss'],
})
export class CssTaskComponent implements OnInit {
	testKitRowLength = 3;
	svgRoot = 'https://lgcassets.com/v-441/assets/3.0/images/icons/';
	homeTestKits = [
		{ name: 'Cholestrol Test', svgIcon: 'cholesterol.svg', description: 'Get an insight into your cardiovascular health, track and improve results.' },
		{ name: 'Thyroid antibody Test', svgIcon: 'thyroid.svg', description: 'See how your thyroid is performing by measuring a few key indicators.' },
		{ name: 'Bowel Cancer Screening Test', svgIcon: 'bowel.svg', description: 'Identifies hidden blood in the stool, which could indicate colon cancer.' },
		{ name: 'Standard STD Test', svgIcon: 'vitamin.svg', description: 'Get peace of mind by testing for 5 of the most common STIs from home.' },
		{ name: 'Male Hormone Test', svgIcon: 'omega.svg', description: 'Check out out your hormone levels, which can help identify imbalances.' },
		{ name: 'Female Hormone Test', svgIcon: 'folate.svg', description: 'This test can help you gain a deeper understanding of your hormonal health.' }
	];
	@HostListener('window:resize', ['$event'])
	private onResize(event: any): void {
		// TODO: this could be more efficient
		const width = event.target.innerWidth;
		this.testKitRowLength = this.calculateKitRowLength(width);
	}
	constructor() {}

	ngOnInit(): void {
		this.testKitRowLength = this.calculateKitRowLength(window.innerWidth);
	}

	calculateKitRowLength(width: number): number {
		// TODO: get actual bootstrap breakpoint by using npm library (768 is taken from Bootstrap breakpoint docs)
		if (width < 768) return 2;
		return 3;
	}
}
