import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { mockPosts } from 'src/app/services/api.service.spec';
import { PostListItemComponent } from './post-list-item.component';

describe('PostListItemComponent', () => {
	let component: PostListItemComponent;
	let fixture: ComponentFixture<PostListItemComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [PostListItemComponent],
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(PostListItemComponent);
		component = fixture.componentInstance;
		component.post = mockPosts[0];
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('should show post data', () => {
		fixture.detectChanges();
		const postElementTitle = fixture.debugElement.query(By.css('mat-card-title'));
		const postElementSubtitle = fixture.debugElement.query(By.css('mat-card-subtitle'));
		const postElementContent = fixture.debugElement.query(By.css('mat-card-content'));
		expect(postElementTitle.nativeElement.innerText).toBe(component.post.title);
		expect(postElementSubtitle.nativeElement.innerText).toBe(`by ${component.post.author} on ${component.post.publish_date}`);
		expect(postElementContent.nativeElement.innerText).toBe(component.post.description);
	});
});
