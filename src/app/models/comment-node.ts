import { Comment } from './comment';

export interface CommentNode extends Comment {
	children: CommentNode[];
}
