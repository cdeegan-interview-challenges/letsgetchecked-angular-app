import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CssTaskComponent } from './components/css-task/css-task.component';
import { PostDetailsComponent } from './components/post-details/post-details.component';
import { PostListComponent } from './components/post-list/post-list.component';

export const routes: Routes = [
	{ path: '', component: PostListComponent },
	{ path: 'post/:id', component: PostDetailsComponent },
	{ path: 'css', component: CssTaskComponent },
	{ path: '**', redirectTo: '' },
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
})
export class AppRoutingModule {}
